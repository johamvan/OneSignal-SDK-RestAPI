﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSignalSDKRestAPI.Client
{
    public class NotificationResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("recipients")]
        public int Recipients { get; set; }
    }
}
