﻿using static OneSignalSDKRestAPI.Utils.Constants;
using static OneSignalSDKRestAPI.Utils.WebClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSignalSDKRestAPI.Client
{
    public class Notification : OneSignalObject
    {
        public string Headings { get; set; }
        public string Contents { get; set; }

        public NotificationResponse CreateNotification(Notification notification)
        {
            var notificationResponse = new NotificationResponse();
            SendRequest(UrlNotifications, notification);
            return notificationResponse;
        }

    }
}
