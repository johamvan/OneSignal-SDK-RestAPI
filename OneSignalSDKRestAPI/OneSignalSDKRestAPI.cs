﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSignalSDKRestAPI
{
    public sealed class OneSignalSDKRestAPI
    {
        public static string AppId { get; set; }
        public static string AuthKey { get; set; }
        
        public static void Init(string appId, string authKey)
        {
            AppId = appId;
            AuthKey = authKey;
        }
    }
}
