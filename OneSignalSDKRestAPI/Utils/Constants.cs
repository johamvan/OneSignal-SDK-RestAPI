﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSignalSDKRestAPI.Utils
{
    public static class Constants
    {

        #region Urls
        const string UrlServerRestApi = "https://onesignal.com/api/v1/";
        public const string UrlNotifications = UrlServerRestApi + "notifications";
        public const string UrlApps = UrlServerRestApi + "apps";
        public const string UrlDevices = UrlServerRestApi + "players";
        #endregion

        #region Others
        public const string POST = "POST";
        public const string GET = "GET";
        public const string DELETE = "DELETE";
        public const string PUT = "PUT";
        #endregion

    }
}
