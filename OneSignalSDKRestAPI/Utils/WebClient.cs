﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using OSDK = OneSignalSDKRestAPI.OneSignalSDKRestAPI;
using static OneSignalSDKRestAPI.Utils.Constants;
using OneSignalSDKRestAPI.Client;
using System.Web.Script.Serialization;
using System.IO;

namespace OneSignalSDKRestAPI.Utils
{
    public class WebClient
    {
        internal WebClient() { }

        static HttpWebRequest CreateRequest(string url, string method = POST)
        {
            var request = WebRequest.Create(url) as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = method;
            request.ContentType = "application/json; charset=utf-8";

            request.Headers.Add("authorization", $"Basic {OSDK.AuthKey}");
            return request;
        }

        public static string SendRequest(string url, OneSignalObject obj, string method = POST)
        {
            var request = CreateRequest(url, method);
            byte[] byteArray = Encoding.UTF8.GetBytes(new JavaScriptSerializer().Serialize(obj));
            try
            {
                string responseContent = null;
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                    }
                }
                return responseContent;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return "An error ocurred! Read Console.";
        }
    }
}
